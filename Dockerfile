FROM golang:alpine AS builder

RUN apk update && apk add --no-cache git
WORKDIR $GOPATH/src/sensedia/
COPY . .

ENV CGO_ENABLED=0
ENV GOOS=linux

RUN go get -d -v
RUN go build -o /go/bin/http-server-go

FROM alpine
COPY --from=builder /go/bin/http-server-go /go/bin/http-server-go
ENTRYPOINT ["/go/bin/http-server-go"]