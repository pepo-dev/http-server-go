package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func healthz(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	if fileExists("/tmp/content.dat") {
		fmt.Fprintf(w, "%+v", "{\"status\": \"ok\"}")
	} else {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "%+v", "{\"status\": \"File doesn't exist.\"}")
	}

}

func hello(w http.ResponseWriter, r *http.Request) {

	addrs, err := net.InterfaceAddrs()
	check(err)

	for _, a := range addrs {
		if ipnet, ok := a.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				fmt.Fprintf(w, "Hello from %+v", ipnet.IP.String())
			}
		}
	}

	fmt.Fprintf(w, " | v2")

}

func handler(w http.ResponseWriter, r *http.Request) {

	dat, err := ioutil.ReadFile("/tmp/content.dat")
	check(err)

	fmt.Fprintf(w, "%+v", string(dat))

}

func main() {

	r := mux.NewRouter()
	r.HandleFunc("/healthz", healthz).Methods("GET")
	r.HandleFunc("/hello", hello).Methods("GET")
	r.HandleFunc("/", handler).Methods("GET")

	log.Fatal(http.ListenAndServe(":8080", r))

}
